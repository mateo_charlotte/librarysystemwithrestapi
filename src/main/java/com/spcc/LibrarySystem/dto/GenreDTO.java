package com.spcc.LibrarySystem.dto;

import com.spcc.LibrarySystem.model.Genre;

public class GenreDTO {
    private Integer idNo;
    private String name;

    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(Integer idNo) {
        this.idNo = idNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GenreDTO(){

    }

    public GenreDTO(Genre genre){
        this.idNo = genre.getIdNo();
        this.name = genre.getName();
    }

    public Genre toEntity(){
        Genre genre = new Genre();
        genre.setIdNo(idNo);
        genre.setName(name);
        return genre;
    }

}
