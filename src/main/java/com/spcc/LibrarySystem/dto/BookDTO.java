package com.spcc.LibrarySystem.dto;

import com.spcc.LibrarySystem.model.Author;
import com.spcc.LibrarySystem.model.Book;
import com.spcc.LibrarySystem.model.Genre;
import com.spcc.LibrarySystem.model.Publisher;

import java.util.Date;
import java.util.List;

public class BookDTO {
    private Integer idNo;

    private String name;

    private Integer qty;

    private Date datePublished;

    private String status;

    private List<Author> authors;

    private List<Genre> genres;

    private  Publisher publisher;

    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(Integer idNo) {
        this.idNo = idNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Date getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(Date datePublished) {
        this.datePublished = datePublished;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public BookDTO() {

    }

    public BookDTO(Book book) {
        this.idNo = book.getIdNo();
        this.name = book.getName();
        this.qty = book.getQty();
        this.status = book.getStatus();
        this.datePublished = book.getDatePublished();
        this.authors = book.getAuthors();
        this.genres = book.getGenres();
        this.publisher = book.getPublisher();
    }

    public Book toEntity() {
        Book book = new Book();
        book.setIdNo(idNo);
        book.setName(name);
        book.setQty(qty);
        book.setStatus(status);
        book.setAuthors(authors);
        book.setGenres(genres);
        book.setPublisher(publisher);
        return book;
    }
}
