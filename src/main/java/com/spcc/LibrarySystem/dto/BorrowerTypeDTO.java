package com.spcc.LibrarySystem.dto;

public class BorrowerTypeDTO {
    private  Integer idNo;

    private String type;

    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(Integer idNo) {
        this.idNo = idNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
