package com.spcc.LibrarySystem.dto;

import com.spcc.LibrarySystem.model.Librarian;

public class LibrarianDTO {
    private Integer idNo;
    private String fullName;
    private String email;
    private String password;

    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(Integer idNo) {
        this.idNo = idNo;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LibrarianDTO() {

    }

    public LibrarianDTO(Librarian librarian) {
        this.idNo = librarian.getIdNo();
        this.fullName = librarian.getFullName();
        this.email = librarian.getEmail();
        this.password = librarian.getPassword();
    }

    public Librarian toEntity() {
        Librarian librarian = new Librarian();
        librarian.setIdNo(idNo);
        librarian.setFullName(fullName);
        librarian.setEmail(email);
        librarian.setPassword(password);
        return librarian;
    }



}
