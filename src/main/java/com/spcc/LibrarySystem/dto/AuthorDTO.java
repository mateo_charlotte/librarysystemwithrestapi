package com.spcc.LibrarySystem.dto;

import com.spcc.LibrarySystem.model.Author;

public class AuthorDTO {
    private Integer idNo;
    private String name;

    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(Integer idNo) {
        this.idNo = idNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AuthorDTO() {

    }

    public AuthorDTO(Author author) {
        this.idNo = author.getIdNo();
        this.name = author.getName();
    }

    public Author toEntity() {
        Author author = new Author();
        author.setIdNo(idNo);
        author.setName(name);
        return author;
    }
}
