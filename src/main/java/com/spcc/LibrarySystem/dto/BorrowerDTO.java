package com.spcc.LibrarySystem.dto;

import com.spcc.LibrarySystem.model.Borrower;
import com.spcc.LibrarySystem.model.BorrowerType;


public class BorrowerDTO {
    private Integer idNo;
    private String fullName;
    private String email;
    private String contactNo;
    private BorrowerType borrowerType;

    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(int idNo) {
        this.idNo = idNo;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public BorrowerType getBorrowerType() {
        return borrowerType;
    }

    public void setBorrowerType(BorrowerType borrowerType) {
        this.borrowerType = borrowerType;
    }

    public BorrowerDTO() {

    }

    public BorrowerDTO(Borrower borrower) {
        this.idNo = borrower.getIdNo();
        this.fullName = borrower.getFullName();
        this.email = borrower.getEmail();
        this.contactNo = borrower.getContactNo();
        this.borrowerType = borrower.getBorrowerType();
    }


    public Borrower toEntity() {
        Borrower borrower = new Borrower();
        borrower.setIdNo(idNo);
        borrower.setFullName(fullName);
        borrower.setEmail(email);
        borrower.setContactNo(contactNo);
        borrower.setBorrowerType(borrowerType);
        return borrower;
    }

}
