package com.spcc.LibrarySystem.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.spcc.LibrarySystem.model.Book;
import com.spcc.LibrarySystem.model.Borrower;
import com.spcc.LibrarySystem.model.Librarian;
import com.spcc.LibrarySystem.model.Transaction;

import java.util.Date;

public class TransactionDTO {
    private int idNo;
    private Date borrowDate;
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm:ss")
    private Date returnDate;
    private Integer qty;
    private Librarian librarian;
    private Borrower borrower;
    private Book book;

    private String status;

    public int getIdNo() {
        return idNo;
    }

    public void setIdNo(int idNo) {
        this.idNo = idNo;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Librarian getLibrarian() {
        return librarian;
    }

    public void setLibrarian(Librarian librarian) {
        this.librarian = librarian;
    }

    public Borrower getBorrower() {
        return borrower;
    }

    public void setBorrower(Borrower borrower) {
        this.borrower = borrower;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public TransactionDTO() {

    }

    public TransactionDTO(Transaction transaction) {
        this.idNo = transaction.getIdNo();
        this.book = transaction.getBook();
        this.qty = transaction.getQty();
        this.status = transaction.getStatus();
        this.borrowDate = transaction.getBorrowDate();
        this.returnDate = transaction.getReturnDate();
        this.borrower = transaction.getBorrower();
        this.librarian = transaction.getLibrarian();

    }

    public Transaction toEntity() {
        Transaction transaction = new Transaction();
        transaction.setIdNo(idNo);
        transaction.setReturnDate(returnDate);
        transaction.setQty(qty);
        transaction.setStatus(status);
        transaction.setBook(book);
        transaction.setBorrower(borrower);
        transaction.setLibrarian(librarian);
        return transaction;
    }


}
