package com.spcc.LibrarySystem.dto;

import com.spcc.LibrarySystem.model.Publisher;

public class PublisherDTO {
    private Integer idNo;
    private String name;

    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(Integer idNo) {
        this.idNo = idNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PublisherDTO(){

    }

    public PublisherDTO(Publisher publisher){
      this.idNo = publisher.getIdNo();
      this.name = publisher.getName();
    }

    public Publisher toEntity(){
        Publisher publisher = new Publisher();
        publisher.setIdNo(idNo);
        publisher.setName(name);
        return publisher;
    }

}

