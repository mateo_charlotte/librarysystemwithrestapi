package com.spcc.LibrarySystem.repository;

import com.spcc.LibrarySystem.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("BookRepository")
public interface BookRepository extends JpaRepository<Book,Integer> {

    Book findBookByIdNo(Integer idNo);

    List<Book> findAll();


    @Query(nativeQuery = true, value = "" +
            " select b.* from " + Book.ENTITY_NAME + " b " +
            " where b.name =:name limit 1"
    )
    Book findBookByName(
            @Param("name") String name
    );


}
