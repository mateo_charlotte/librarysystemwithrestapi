package com.spcc.LibrarySystem.repository;

import com.spcc.LibrarySystem.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Integer> {

    Genre findGenreByIdNo(int idNo);

}
