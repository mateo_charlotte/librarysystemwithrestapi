package com.spcc.LibrarySystem.repository;

import com.spcc.LibrarySystem.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Integer> {
   Author findAuthorByIdNo(int idNo);

}
