package com.spcc.LibrarySystem.repository;

import com.spcc.LibrarySystem.model.Borrower;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

@Repository
public interface BorrowerRepository extends JpaRepository<Borrower,Integer> {

    Borrower findBorrowerByIdNo(int idNo);

}
