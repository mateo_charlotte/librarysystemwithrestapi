package com.spcc.LibrarySystem.repository;

import com.spcc.LibrarySystem.model.Librarian;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LibrarianRepository extends JpaRepository<Librarian,Integer> {
    Librarian findLibrarianByIdNo(Integer idNo);
    List<Librarian> findAllByOrderByFullName();

}
