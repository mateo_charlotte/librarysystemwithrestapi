package com.spcc.LibrarySystem.repository;

import com.spcc.LibrarySystem.model.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Integer> {

    Publisher findPublisherByIdNo(int idNo);
}
