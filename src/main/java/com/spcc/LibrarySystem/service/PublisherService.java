package com.spcc.LibrarySystem.service;

import com.spcc.LibrarySystem.dto.PublisherDTO;
import com.spcc.LibrarySystem.exception.NullEntityUpdateException;
import com.spcc.LibrarySystem.model.Publisher;
import com.spcc.LibrarySystem.repository.PublisherRepository;
import com.spcc.LibrarySystem.service.interfaces.IPublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PublisherService implements IPublisherService {

    private final PublisherRepository publisherRepository;

    @Autowired
    public PublisherService(PublisherRepository publisherRepository) {
        this.publisherRepository = publisherRepository;
    }

    @Override
    public List<PublisherDTO> getAllPublisher() {
        return parseDTOList(publisherRepository.findAll());
    }

    private List<PublisherDTO> parseDTOList(List<Publisher> publisher) {
        return publisher.stream().map(PublisherDTO::new).collect(Collectors.toList());
    }

    @Override
    public PublisherDTO getPublisherByIdNo(int idNo) {
        return tryFindPublisherById(publisherRepository.findPublisherByIdNo(idNo));
    }

    private PublisherDTO tryFindPublisherById(Publisher publisher) {
        try {
            PublisherDTO publisherDTO = new PublisherDTO();
            publisherDTO.setIdNo(publisher.getIdNo());
            publisherDTO.setName(publisher.getName());
            return publisherDTO;
        } catch (Exception e) {
            e.printStackTrace();
            throw new NullEntityUpdateException();
        }
    }

    @Override
    public void addPublisher(PublisherDTO publisherDTO) {
        tryAddPublisher(publisherDTO);
    }

    private void tryAddPublisher(PublisherDTO publisherDTO) {
        try {
            Publisher publisher = publisherDTO.toEntity();
            publisher.setIdNo(null);
            publisherRepository.save(publisher);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAuthor(int idNo, PublisherDTO publisherDTO) {
        Publisher publisher = publisherRepository.findPublisherByIdNo(idNo);
        if (publisher == null) {
            throw new NullEntityUpdateException();
        } else {
            publisher.copyValues(publisherDTO.toEntity());
            publisher.setIdNo(idNo);
            publisherRepository.save(publisher);
        }
    }

    @Override
    public void deletePublisherByIdNo(int idNo) {
        publisherRepository.deleteById(idNo);
    }
}
