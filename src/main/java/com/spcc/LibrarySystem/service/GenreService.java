package com.spcc.LibrarySystem.service;

import com.spcc.LibrarySystem.dto.GenreDTO;
import com.spcc.LibrarySystem.exception.NullEntityUpdateException;
import com.spcc.LibrarySystem.model.Genre;
import com.spcc.LibrarySystem.repository.GenreRepository;
import com.spcc.LibrarySystem.service.interfaces.IGenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GenreService implements IGenreService {

    private final GenreRepository genreRepository;

    @Autowired
    public GenreService(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Override
    public List<GenreDTO> getAllGenre() {
        return parseDTOList(genreRepository.findAll());
    }

    private List<GenreDTO> parseDTOList(List<Genre> genre) {
        return genre.stream().map(GenreDTO::new).collect(Collectors.toList());
    }

    @Override
    public GenreDTO getGenreByIdNo(int idNo) {
        return tryFindGenreById(genreRepository.findGenreByIdNo(idNo));
    }

    private GenreDTO tryFindGenreById(Genre genre) {
        try {
            GenreDTO genreDTO = new GenreDTO();
            genreDTO.setIdNo(genre.getIdNo());
            genreDTO.setName(genre.getName());
            return genreDTO;
        } catch (Exception e) {
            e.printStackTrace();
            throw new NullEntityUpdateException();
        }
    }

    @Override
    public void addGenre(GenreDTO genreDTO) {
        tryAddGenre(genreDTO);
    }

    private void tryAddGenre(GenreDTO genreDTO) {
        try {
            Genre genre = genreDTO.toEntity();
            genre.setIdNo(null);
            genreRepository.save(genre);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateGenre(int idNo, GenreDTO genreDTO) {
        Genre genre = genreRepository.findGenreByIdNo(idNo);
        if (genre == null) {
            throw new NullEntityUpdateException();
        } else {
            genre.copyValues(genreDTO.toEntity());
            genre.setIdNo(idNo);
            genreRepository.save(genre);
        }
    }

    @Override
    public void deleteGenreByIdNo(int idNo) {
        genreRepository.deleteById(idNo);
    }
}
