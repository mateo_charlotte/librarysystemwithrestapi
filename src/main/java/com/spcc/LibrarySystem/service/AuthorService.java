package com.spcc.LibrarySystem.service;

import com.spcc.LibrarySystem.dto.AuthorDTO;
import com.spcc.LibrarySystem.exception.NullEntityUpdateException;
import com.spcc.LibrarySystem.model.Author;
import com.spcc.LibrarySystem.repository.AuthorRepository;
import com.spcc.LibrarySystem.service.interfaces.IAuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthorService implements IAuthorService {

    private final AuthorRepository authorRepository;

    @Autowired
    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public List<AuthorDTO> getAllAuthor() {
        return parseDTOList(authorRepository.findAll());
    }

    private List<AuthorDTO> parseDTOList(List<Author> author) {
        return author.stream().map(AuthorDTO::new).collect(Collectors.toList());
    }

    @Override
    public AuthorDTO getAuthorByIdNo(int idNo) {
        return tryFindAuthorByIdNo(idNo);
    }

    private AuthorDTO tryFindAuthorByIdNo(int idNo) {
        try {
            Author author = authorRepository.findAuthorByIdNo(idNo);
            AuthorDTO authorDTO = new AuthorDTO();
            authorDTO.setIdNo(author.getIdNo());
            authorDTO.setName(author.getName());
            return authorDTO;
        } catch (Exception e) {
            e.printStackTrace();
            throw new NullEntityUpdateException();
        }
    }

    @Override
    public void addAuthor(AuthorDTO authorDTO) {
        tryAddAuthor(authorDTO);
    }

    private void tryAddAuthor(AuthorDTO authorDTO) {
        try {
            Author author = authorDTO.toEntity();
            author.setIdNo(null);
            authorRepository.save(author);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateAuthor(Integer idNo, AuthorDTO authorDTO) {
        Author author = authorRepository.findAuthorByIdNo(idNo);
        if (author == null) {
            throw new NullEntityUpdateException();
        } else {
            author.copyValues(authorDTO.toEntity());
            author.setIdNo(idNo);
            authorRepository.save(author);
        }
    }

    @Override
    public void deleteAuthorByIdNo(Integer idNo) {
        authorRepository.deleteById(idNo);
    }
}
