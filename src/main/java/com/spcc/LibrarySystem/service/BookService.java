package com.spcc.LibrarySystem.service;

import com.spcc.LibrarySystem.dto.BookDTO;
import com.spcc.LibrarySystem.exception.NullEntityUpdateException;
import com.spcc.LibrarySystem.model.Book;
import com.spcc.LibrarySystem.repository.BookRepository;
import com.spcc.LibrarySystem.service.interfaces.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService implements IBookService {

    private final BookRepository bookRepository;

    @Autowired
    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<BookDTO> getAllBook() {
            return parseDTOList(bookRepository.findAll());
    }

    @Override
    public BookDTO getBookByName(String name) {
       Book book = bookRepository.findBookByName(name);
       BookDTO bookDTO = new BookDTO();
       bookDTO.setIdNo(book.getIdNo());
       bookDTO.setName(book.getName());
       bookDTO.setDatePublished(book.getDatePublished());
       bookDTO.setQty(book.getQty());
       bookDTO.setGenres(book.getGenres());
       book.setAuthors(book.getAuthors());
       book.setPublisher(book.getPublisher());
       return bookDTO;
    }

    private List<BookDTO> parseDTOList(List<Book> book) {
        return book.stream().map(BookDTO::new).collect(Collectors.toList());
    }

    @Override
    public void addBook(BookDTO bookDTO) {
        try{
            Book book = bookDTO.toEntity();
            book.setIdNo(null);
            book.setDatePublished(Date.from(Instant.now()));
            bookRepository.save(book);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }

    @Override
    public void updateBook(Integer idNo, BookDTO bookDTO) {
        Book book = bookRepository.findBookByIdNo(idNo);
        if(book == null) {
            throw new NullEntityUpdateException();
        }else {
            book.copyValues(bookDTO.toEntity());
            book.setIdNo(idNo);
            book.setDatePublished(Timestamp.from(Instant.now()));
            bookRepository.save(book);
        }
    }

    @Override
    public void deleteBookByIdNo(Integer idNo) {
        bookRepository.deleteById(idNo);
    }
}
