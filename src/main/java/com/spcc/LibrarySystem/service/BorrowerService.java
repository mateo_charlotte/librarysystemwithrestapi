package com.spcc.LibrarySystem.service;

import com.spcc.LibrarySystem.dto.BorrowerDTO;
import com.spcc.LibrarySystem.exception.NullEntityUpdateException;
import com.spcc.LibrarySystem.model.Borrower;
import com.spcc.LibrarySystem.repository.BorrowerRepository;
import com.spcc.LibrarySystem.service.interfaces.IBorrowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BorrowerService implements IBorrowerService {

    private final BorrowerRepository borrowerRepository;

    @Autowired
    public BorrowerService(BorrowerRepository borrowerRepository) {
        this.borrowerRepository = borrowerRepository;
    }

    @Override
    public List<BorrowerDTO> getAllBorrower() {
        return parseDTOList(borrowerRepository.findAll());
    }

    private List<BorrowerDTO> parseDTOList(List<Borrower> borrower) {
        return borrower.stream().map(BorrowerDTO::new).collect(Collectors.toList());
    }

    @Override
    public BorrowerDTO getBorrowerByIdNo(int idNo) {
        return tryFindBorrowerById(borrowerRepository.findBorrowerByIdNo(idNo));
    }

    private BorrowerDTO tryFindBorrowerById(Borrower borrower) {
        try {
            BorrowerDTO borrowerDTO = new BorrowerDTO();
            borrowerDTO.setIdNo(borrower.getIdNo());
            borrowerDTO.setFullName(borrower.getFullName());
            borrowerDTO.setEmail(borrower.getEmail());
            borrowerDTO.setContactNo(borrower.getContactNo());
            borrowerDTO.setBorrowerType(borrower.getBorrowerType());
            return borrowerDTO;
        } catch (Exception e) {
            e.printStackTrace();
            throw new NullEntityUpdateException();
        }
    }

    @Override
    public void addBorrower(BorrowerDTO borrowerDTO) {
        try{
            Borrower borrower = borrowerDTO.toEntity();
            borrower.setIdNo(null);
            borrowerRepository.save(borrower);
        }catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    public void updateBorrower(int idNo, BorrowerDTO borrowerDTO) {
        Borrower borrower =  borrowerRepository.findBorrowerByIdNo(idNo);
        if (borrower == null) {
            throw new NullEntityUpdateException();
        } else {
            borrower.copyValues(borrowerDTO.toEntity());
            borrower.setIdNo(idNo);
            borrower.setBorrowerType(borrowerDTO.getBorrowerType());
            borrowerRepository.save(borrower);
        }
    }

    @Override
    public void deleteBorrowerByIdNo(int idNo) {
        borrowerRepository.deleteById(idNo);
    }
}
