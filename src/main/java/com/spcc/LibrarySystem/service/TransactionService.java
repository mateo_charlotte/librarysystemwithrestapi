package com.spcc.LibrarySystem.service;

import com.spcc.LibrarySystem.dto.TransactionDTO;
import com.spcc.LibrarySystem.model.Transaction;
import com.spcc.LibrarySystem.repository.TransactionRepository;
import com.spcc.LibrarySystem.service.interfaces.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionService implements ITransactionService {

    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionService(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @Override
    public List<TransactionDTO> getAllTransactions() {
        return parseDTOList(transactionRepository.findAll());
    }

    private List<TransactionDTO> parseDTOList(List<Transaction> transaction) {
        return transaction.stream().map(TransactionDTO::new).collect(Collectors.toList());
    }

    @Override
    public TransactionDTO getTransactionByIdNo() {
        return null;
    }

    @Override
    public void addTransaction(TransactionDTO transactionDTO) {
        tryAddTransaction(transactionDTO);
    }

    private void tryAddTransaction(TransactionDTO transactionDTO) {
        try {
            Transaction transaction = transactionDTO.toEntity();
            transaction.setIdNo(null);
            transaction.setBorrowDate(Timestamp.from(Instant.now()));
            transaction.setReturnDate(Timestamp.from(Instant.now()));
            transactionRepository.save(transaction);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void deleteTransactionByIdNo(int idNo) {
         transactionRepository.deleteById(idNo);
    }


}
