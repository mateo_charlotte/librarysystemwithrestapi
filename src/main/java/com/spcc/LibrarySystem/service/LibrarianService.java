package com.spcc.LibrarySystem.service;

import com.spcc.LibrarySystem.dto.LibrarianDTO;
import com.spcc.LibrarySystem.exception.NullEntityUpdateException;
import com.spcc.LibrarySystem.model.Librarian;
import com.spcc.LibrarySystem.repository.LibrarianRepository;
import com.spcc.LibrarySystem.service.interfaces.ILibrarianService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LibrarianService implements ILibrarianService {

    private LibrarianRepository librarianRepository;

    private final Logger logger = LogManager.getLogger(LibrarianService.class);


    @Autowired
    public LibrarianService(LibrarianRepository librarianRepository) {
        this.librarianRepository = librarianRepository;
    }

    @Override
    public List<LibrarianDTO> getAllLibrarian() {
        return parseDTOList(librarianRepository.findAllByOrderByFullName());
    }

    private List<LibrarianDTO> parseDTOList(List<Librarian> librarian) {
        return librarian.stream().map(LibrarianDTO::new).collect(Collectors.toList());
    }

    @Override
    public LibrarianDTO getLibrarianByIdNo(Integer librarianIdNo) {
        return null;
    }

    @Override
    public void addLibrarian(LibrarianDTO librarianDTO) {
        Librarian librarian = librarianDTO.toEntity();
        librarian.setIdNo(null);
        librarianRepository.save(librarian);
    }

    @Override
    public void updateLibrarian(Integer idNo, LibrarianDTO librarianDTO) {
        Librarian librarian = librarianRepository.findLibrarianByIdNo(idNo);
        if(librarian == null){
            throw  new NullEntityUpdateException();
        }else{
            librarian.copyValues(librarianDTO.toEntity());
            librarian.setIdNo(idNo);
            librarianRepository.save(librarian);
        }
    }

    @Override
    public void deleteLibrarianDTOByIdNo(Integer idNo) {
        librarianRepository.deleteById(idNo);
    }
}
