package com.spcc.LibrarySystem.service.interfaces;

import com.spcc.LibrarySystem.dto.BorrowerDTO;

import java.util.List;

public interface IBorrowerService {
    List<BorrowerDTO> getAllBorrower();

    BorrowerDTO getBorrowerByIdNo(int idNo);

    void addBorrower(BorrowerDTO borrowerDTO);

    void updateBorrower(int idNo, BorrowerDTO borrowerDTO);

    void deleteBorrowerByIdNo(int idNo);
}
