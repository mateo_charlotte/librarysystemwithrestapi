package com.spcc.LibrarySystem.service.interfaces;

import com.spcc.LibrarySystem.dto.BookDTO;


import java.util.List;

public interface IBookService {

    List<BookDTO> getAllBook();

    BookDTO getBookByName(String Name);

    void addBook(BookDTO bookDTO);

    void updateBook(Integer idNo, BookDTO bookDTO);

    void deleteBookByIdNo(Integer idNo);

}
