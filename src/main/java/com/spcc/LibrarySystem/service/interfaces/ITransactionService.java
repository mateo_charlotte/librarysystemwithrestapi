package com.spcc.LibrarySystem.service.interfaces;

import com.spcc.LibrarySystem.dto.TransactionDTO;

import java.util.List;

public interface ITransactionService {

    List<TransactionDTO> getAllTransactions();

    TransactionDTO getTransactionByIdNo();

    void addTransaction(TransactionDTO transactionDTO);

    void deleteTransactionByIdNo(int idNo);

}
