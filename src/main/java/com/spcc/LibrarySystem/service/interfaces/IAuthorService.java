package com.spcc.LibrarySystem.service.interfaces;

import com.spcc.LibrarySystem.dto.AuthorDTO;

import java.util.List;

public interface IAuthorService {

    List<AuthorDTO> getAllAuthor();

    AuthorDTO getAuthorByIdNo(int idNo);

    void addAuthor(AuthorDTO authorDTO);

    void updateAuthor(Integer idNo, AuthorDTO authorDTO);

    void deleteAuthorByIdNo(Integer idNo);

}
