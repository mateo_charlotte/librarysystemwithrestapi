package com.spcc.LibrarySystem.service.interfaces;

import com.spcc.LibrarySystem.dto.GenreDTO;

import java.util.List;

public interface IGenreService {
    List<GenreDTO> getAllGenre();

    GenreDTO getGenreByIdNo(int idNo);

    void addGenre(GenreDTO genreDTO);

    void updateGenre(int idNo, GenreDTO genreDTO);

    void deleteGenreByIdNo(int idNo);
}
