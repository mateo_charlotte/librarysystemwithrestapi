package com.spcc.LibrarySystem.service.interfaces;

import com.spcc.LibrarySystem.dto.PublisherDTO;

import java.util.List;

public interface IPublisherService {
    List<PublisherDTO> getAllPublisher();

    PublisherDTO getPublisherByIdNo(int idNo);

    void addPublisher(PublisherDTO publisherDTO);

    void updateAuthor(int idNo, PublisherDTO publisherDTO);

    void deletePublisherByIdNo(int idNo);
}
