package com.spcc.LibrarySystem.service.interfaces;

import com.spcc.LibrarySystem.dto.LibrarianDTO;

import java.util.List;

public interface ILibrarianService {
    List<LibrarianDTO> getAllLibrarian();

    LibrarianDTO getLibrarianByIdNo(Integer librarianIdNo);

    void addLibrarian(LibrarianDTO librarianDTO);

    void updateLibrarian(Integer idNo, LibrarianDTO librarianDTO);

    void deleteLibrarianDTOByIdNo(Integer idNo);
}
