package com.spcc.LibrarySystem.controller;

import com.spcc.LibrarySystem.dto.PublisherDTO;
import com.spcc.LibrarySystem.service.interfaces.IPublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("publisher")
public class PublisherController {

    private final IPublisherService publisherService;

    @Autowired
    public PublisherController(IPublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @GetMapping("/")
    public List<PublisherDTO> getALLPublisher() {
        return publisherService.getAllPublisher();
    }

    @GetMapping("/{idNo}")
    public PublisherDTO getPublisherByIdNo(@PathVariable int idNo) {
        return publisherService.getPublisherByIdNo(idNo);
    }

    @PostMapping("/")
    public void addPublisher(@RequestBody PublisherDTO publisherDTO) {
        publisherService.addPublisher(publisherDTO);
    }

    @PutMapping("/{idNo}")
    public void updatePublisher(@PathVariable int idNo, @RequestBody PublisherDTO publisherDTO) {
        publisherService.updateAuthor(idNo, publisherDTO);
    }

    @DeleteMapping("/{idNo}")
    public void deletePublisher(@PathVariable int idNo){
        publisherService.deletePublisherByIdNo(idNo);
    }
}
