package com.spcc.LibrarySystem.controller;

import com.spcc.LibrarySystem.dto.AuthorDTO;
import com.spcc.LibrarySystem.dto.BookDTO;
import com.spcc.LibrarySystem.service.interfaces.IAuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("author")
public class AuthorController {
    private final IAuthorService authorService;

    @Autowired
    public AuthorController(IAuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping("/")
    public List<AuthorDTO> getAllAuthor() {
        return authorService.getAllAuthor();
    }

    @GetMapping("/{idNo}")
    public AuthorDTO getAuthorByIdNo(@PathVariable int idNo) {
        return authorService.getAuthorByIdNo(idNo);
    }

    @PostMapping("/")
    public void addAuthor(@RequestBody AuthorDTO authorDTO) {
        authorService.addAuthor(authorDTO);
    }

    @PutMapping("/{idNo}")
    public void updateAuthor(@PathVariable int idNo, @RequestBody AuthorDTO authorDTO) {
        authorService.updateAuthor(idNo, authorDTO);
    }

    @DeleteMapping("/{idNo}")
    public void deleteAuthorByIdNo(@PathVariable int idNo) {
        authorService.deleteAuthorByIdNo(idNo);
    }
}
