package com.spcc.LibrarySystem.controller;

import com.spcc.LibrarySystem.dto.BookDTO;
import com.spcc.LibrarySystem.service.interfaces.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("book")
public class BookController {

    private final IBookService bookService;

    @Autowired
    public BookController(IBookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("/")
    public List<BookDTO> getAllBook() {
        return bookService.getAllBook();
    }

    @GetMapping("/bookName")
    public BookDTO getBookByName(@RequestBody BookDTO bookDTO) {
        return bookService.getBookByName(bookDTO.getName());
    }

    @PostMapping("/")
    public void addBook(@RequestBody BookDTO bookDto) {
        bookService.addBook(bookDto);
    }

    @PutMapping("/{idNo}")
    public void updateBook(@PathVariable Integer idNo, @RequestBody BookDTO bookDto) {
        bookService.updateBook(idNo, bookDto);
    }

    @DeleteMapping("/{idNo}")
    public void deleteBookByIdNo(@PathVariable Integer idNo) {
        bookService.deleteBookByIdNo(idNo);
    }


}
