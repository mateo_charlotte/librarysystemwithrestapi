package com.spcc.LibrarySystem.controller;

import com.spcc.LibrarySystem.dto.LibrarianDTO;
import com.spcc.LibrarySystem.service.interfaces.ILibrarianService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("librarian")
public class LibrarianController {

    private final ILibrarianService librarianService;

    @Autowired
    public LibrarianController(ILibrarianService librarianService) {
        this.librarianService = librarianService;
    }

    @GetMapping("/")
    public List<LibrarianDTO> getAllLibrarian() {
        return librarianService.getAllLibrarian();
    }

    @PostMapping("/")
    public  void addLibrarian(@RequestBody LibrarianDTO LIbrarianDTO){
        librarianService.addLibrarian(LIbrarianDTO);
    }

    @PutMapping("/{idNo}")
    public void updateLibrarian(@PathVariable Integer idNo, @RequestBody LibrarianDTO librarianDTO){
        librarianService.updateLibrarian(idNo, librarianDTO);
    }

    @DeleteMapping("/{idNo}")
    public void deleteCountry(@PathVariable Integer idNo) {
        librarianService.deleteLibrarianDTOByIdNo(idNo);
    }


}
