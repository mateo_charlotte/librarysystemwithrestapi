package com.spcc.LibrarySystem.controller;

import com.spcc.LibrarySystem.dto.GenreDTO;
import com.spcc.LibrarySystem.service.interfaces.IGenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("genre")
public class GenreController {

    private final IGenreService genreService;

    @Autowired
    public GenreController(IGenreService genreService) {
        this.genreService = genreService;
    }

    @GetMapping("/")
    public List<GenreDTO> getALLGenre() {
        return genreService.getAllGenre();
    }

    @GetMapping("/{idNo}")
    public GenreDTO getGenreByIdNo(@PathVariable int idNo) {
        return genreService.getGenreByIdNo(idNo);
    }

    @PostMapping("/")
    public void addGenre(@RequestBody GenreDTO genreDTO) {
        genreService.addGenre(genreDTO);
    }

    @PutMapping("/{idNo}")
    public void updateGenre(@PathVariable int idNo, @RequestBody GenreDTO genreDTO) {
        genreService.updateGenre(idNo, genreDTO);
    }

    @DeleteMapping("/{idNo}")
    public void deleteGenre(@PathVariable int idNo){
        genreService.deleteGenreByIdNo(idNo);
    }

}
