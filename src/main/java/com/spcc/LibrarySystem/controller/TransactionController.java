package com.spcc.LibrarySystem.controller;

import com.spcc.LibrarySystem.dto.TransactionDTO;
import com.spcc.LibrarySystem.service.interfaces.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("transaction")
public class TransactionController {

    private final ITransactionService transactionService;

    @Autowired
    public TransactionController(ITransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping("/")
    public List<TransactionDTO> getAllTransactions() {
        return transactionService.getAllTransactions();
    }

    @PostMapping("/")
    public void addTransaction(@RequestBody TransactionDTO transactionDTO) {
        transactionService.addTransaction(transactionDTO);
    }

    @DeleteMapping("/{idNo}")
    public void deleteTransactionByIdNo(@PathVariable int idNo) {
        transactionService.deleteTransactionByIdNo(idNo);
    }
}
