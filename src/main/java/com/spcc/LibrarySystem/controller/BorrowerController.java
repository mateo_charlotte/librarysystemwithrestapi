package com.spcc.LibrarySystem.controller;

import com.spcc.LibrarySystem.dto.BorrowerDTO;
import com.spcc.LibrarySystem.service.interfaces.IBorrowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("borrower")
public class BorrowerController {
    private final IBorrowerService borrowerService;

    @Autowired
    public BorrowerController(IBorrowerService borrowerService) {
        this.borrowerService = borrowerService;
    }

    @GetMapping("/")
    public List<BorrowerDTO> getAllBorrower() {
        return borrowerService.getAllBorrower();
    }

    @GetMapping("/{idNo}")
    public BorrowerDTO getBorrower(@PathVariable int idNo) {
        return borrowerService.getBorrowerByIdNo(idNo);
    }

    @PostMapping("/")
    public void addBorrower(@RequestBody BorrowerDTO borrowerDTO) {
        borrowerService.addBorrower(borrowerDTO);
    }

    @PutMapping("/{idNo}")
    public void updateBorrower(@PathVariable int idNo, @RequestBody BorrowerDTO borrowerDTO) {
        borrowerService.updateBorrower(idNo, borrowerDTO);
    }

    @DeleteMapping("/{idNo}")
    public void deleteBorrower(@PathVariable int idNo){
       borrowerService.deleteBorrowerByIdNo(idNo);
    }
}
