package com.spcc.LibrarySystem.ApiHelper;

import java.net.URI;
import java.net.http.HttpRequest;

public class ApiHelper {

    private final String HEADER_NAME = "Content-Type";
    private final String HEADER_VALUE = "application/json; charset=utf8";

    public HttpRequest get(String url) throws Exception {
        return HttpRequest.newBuilder()
                .uri(new URI(url))
                .GET()
                .build();
    }

    public HttpRequest post(String url, String jsonString) throws Exception {
        return HttpRequest.newBuilder()
                .uri(new URI(url))
                .header(HEADER_NAME, HEADER_VALUE)
                .POST(HttpRequest.BodyPublishers.ofString(jsonString))
                .build();
    }

    public HttpRequest put(String url, String jsonString) throws Exception {
        return HttpRequest.newBuilder()
                .uri(new URI(url))
                .header(HEADER_NAME, HEADER_VALUE)
                .PUT(HttpRequest.BodyPublishers.ofString(jsonString))
                .build();
    }

    public HttpRequest delete(String url) throws Exception {
        return HttpRequest.newBuilder()
                .uri(new URI(url))
                .DELETE()
                .build();
    }

}
