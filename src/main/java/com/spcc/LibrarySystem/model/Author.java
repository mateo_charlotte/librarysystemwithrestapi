package com.spcc.LibrarySystem.model;


import javax.persistence.*;
import java.util.List;

@Entity(name = Author.ENTITY_NAME)
@Table(name =  Author.ENTITY_NAME)
public class Author {
    public  static final String ENTITY_NAME = "author";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idNo;

    @Column
    private String name;

    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy = "Authors")
    private List<Book> books;

    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(Integer idNo) {
        this.idNo = idNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public void copyValues(Author author){
        this.name = author.getName();
    }




}
