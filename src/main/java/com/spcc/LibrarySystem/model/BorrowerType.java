package com.spcc.LibrarySystem.model;

import javax.persistence.*;

@Entity(name = BorrowerType.ENTITY_NAME)
@Table(name = BorrowerType.ENTITY_NAME)
public class BorrowerType {
    public  static final String ENTITY_NAME = "borrowerType";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer idNo;

    @Column
    private String type;


    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(Integer idNo) {
        this.idNo = idNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public  void copyValues(BorrowerType borrowerType) {
        this.type = borrowerType.getType();
    }
}
