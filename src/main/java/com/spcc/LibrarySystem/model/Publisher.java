package com.spcc.LibrarySystem.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity(name = Publisher.ENTITY_NAME)
@Table(name = Publisher.ENTITY_NAME)
public class Publisher {
    public  static final String ENTITY_NAME = "publisher";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer idNo;

    @Column
    private String name;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "publisher",
            cascade = CascadeType.ALL)
    private List<Book> books;

    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(Integer idNo) {
        this.idNo = idNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public void copyValues(Publisher publisher){
        this.name = publisher.getName();
    }


}
