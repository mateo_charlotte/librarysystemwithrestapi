package com.spcc.LibrarySystem.model;

import javax.persistence.*;

@Entity(name = Librarian.ENTITY_NAME)
@Table(name = Librarian.ENTITY_NAME)
public class Librarian {

    public static final String ENTITY_NAME = "librarian";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer idNo;

    @Column
    private String fullName;

    @Column
    private String email;

    @Column
    private String password;

    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(Integer idNo) {
        this.idNo = idNo;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void copyValues(Librarian librarian) {
        this.fullName = librarian.getFullName();
        this.email = librarian.getEmail();
        this.password = librarian.getPassword();
    }


}
