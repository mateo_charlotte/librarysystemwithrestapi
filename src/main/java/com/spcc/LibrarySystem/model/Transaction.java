package com.spcc.LibrarySystem.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity(name = Transaction.ENTITY_NAME)
@Table(name = Transaction.ENTITY_NAME)
public class Transaction {

    public  static final String ENTITY_NAME = "transaction";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer idNo;

    @Column
    private Date borrowDate;

    @Column()
    private Date returnDate;

    @Column
    private Integer qty;

    @Column
    private String status;


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "transacBy_id", nullable = false)
    private Librarian librarian;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "borrower_id", nullable = false)
    private Borrower borrower;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "book_id", nullable = false)
    private Book book;

    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(Integer idNo) {
        this.idNo = idNo;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Borrower getBorrower() {
        return borrower;
    }

    public void setBorrower(Borrower borrower) {
        this.borrower = borrower;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Librarian getLibrarian() {
        return librarian;
    }

    public void setLibrarian(Librarian librarian) {
        this.librarian = librarian;
    }

    public void copyValues(Transaction transaction){
        this.borrowDate = transaction.getBorrowDate();
        this.returnDate = transaction.getReturnDate();
        this.qty = transaction.getQty();
        this.status = transaction.getStatus();
    }
}
