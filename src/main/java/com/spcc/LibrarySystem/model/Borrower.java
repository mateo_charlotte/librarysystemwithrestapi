package com.spcc.LibrarySystem.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;


@Entity(name = Borrower.ENTITY_NAME)
@Table(name = Borrower.ENTITY_NAME)
public class Borrower {
    public  static final String ENTITY_NAME = "borrower";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer idNo;

    @Column
    private String fullName;

    @Column
    private String email;

    @Column
    private String contactNo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "borrowerType_id")
    private BorrowerType borrowerType;

    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(Integer idNo) {
        this.idNo = idNo;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public BorrowerType getBorrowerType() {
        return borrowerType;
    }

    public void setBorrowerType(BorrowerType borrowerType) {
        this.borrowerType = borrowerType;
    }

    public void copyValues(Borrower borrower){
        this.fullName = borrower.getFullName();
        this.email = borrower.getEmail();
        this.contactNo = borrower.getContactNo();
    }





}
