package com.spcc.LibrarySystem.model;

import com.fasterxml.jackson.annotation.JsonIgnore;


import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity(name = Book.ENTITY_NAME)
@Table(name = Book.ENTITY_NAME)

public class Book {
    public static final String ENTITY_NAME = "book";
    public static final String BOOK_GENRE_MAPPING = "bookGenre";
    public static final String BOOK_AUTHOR_MAPPING = "bookAuthor";

    public static final String GENRE_FOREIGN_KEY = "genreIdNo";
    public static final String AUTHOR_FOREIGN_KEY = "authorIdNo";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer idNo;

    @Column
    private String name;

    @Column
    private Integer qty;

    @Column
    private Date datePublished;

    @Column
    private String status;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "bookAuthor",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id"))
    private List<Author> Authors;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = BOOK_GENRE_MAPPING,
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "genre_id"))
    private List<Genre> genres;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "publisher_id")
    private Publisher publisher;


    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(Integer idNo) {
        this.idNo = idNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Date getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(Date datePublished) {
        this.datePublished = datePublished;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonIgnore
    public List<Author> getAuthors() {
        return Authors;
    }

    public void setAuthors(List<Author> authors) {
        Authors = authors;
    }

    @JsonIgnore
    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    @JsonIgnore
    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public void copyValues(Book book) {
        this.name = book.getName();
        this.qty = book.getQty();
        this.datePublished = book.getDatePublished();
        this.status = book.getStatus();
        this.publisher = book.getPublisher();
        this.Authors = book.getAuthors();
        this.genres = book.getGenres();
    }


}
