package com.spcc.LibrarySystem.model;

import javax.persistence.*;
import java.util.List;

@Entity(name = Genre.ENTITY_NAME)
@Table(name = Genre.ENTITY_NAME)
public class Genre {
    public  static final String ENTITY_NAME = "genre";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Integer idNo;

    @Column
    private String  name;

    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy = "genres")
    private List<Book> books;

    public Integer getIdNo() {
        return idNo;
    }

    public void setIdNo(Integer idNo) {
        this.idNo = idNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public void copyValues(Genre genre) {
        this.name = genre.getName();
        this.books = genre.getBooks();
    }

}
