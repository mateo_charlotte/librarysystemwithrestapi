package com.spcc.LibrarySystem.view.BorrowerRecord;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spcc.LibrarySystem.ApiHelper.ApiHelper;
import com.spcc.LibrarySystem.dto.BorrowerDTO;
import com.spcc.LibrarySystem.model.BorrowerType;

import java.net.http.HttpClient;
import java.net.http.HttpResponse;

public class BorrowerInformationClass {

    private final ApiHelper apiHelper = new ApiHelper();
    private String url = "http://localhost:8080/borrower/";
    private final BorrowerDTO borrowerDTO = new BorrowerDTO();
    private String idNo = "";

    public void setContactStringIdNoToUrl(String ContactStringIdNoToUrl) {
        this.idNo = ContactStringIdNoToUrl;
    }

    public BorrowerDTO[] sendBorrowerGetRequest() throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response = httpClient.send(apiHelper.get(url),
                HttpResponse.BodyHandlers.ofString());
        String json = response.body();
        Gson gson = new Gson();
        BorrowerDTO[] borrower = gson.fromJson(json, BorrowerDTO[].class);
        return borrower;
    }

    public BorrowerDTO sendBorrowerGetRequestByIdNo(String idNo) throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response = httpClient.send(apiHelper.get(url.concat(idNo)),
                HttpResponse.BodyHandlers.ofString());
        String json = response.body();
        Gson gson = new Gson();
        BorrowerDTO borrower = gson.fromJson(json, BorrowerDTO.class);
        return borrower;
    }

    public HttpResponse sendPutRequest() throws Exception {
        ContactStringIdNoToUrl(borrowerDTO.getIdNo().toString());
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> putResponse = httpClient.send(
                apiHelper.put(url, convertBorrowerToJsonString()),
                HttpResponse.BodyHandlers.ofString());
        return putResponse;
    }

    public void ContactStringIdNoToUrl(String idNo) {
        url = url.concat(idNo);
    }

    public BorrowerDTO setBorrowerInfo(String[] borrowerInfo,
            int borrowerTypeIdNo) {
        BorrowerType b = new BorrowerType();
        b.setIdNo(borrowerTypeIdNo + 1);
        borrowerDTO.setIdNo(Integer.parseInt(borrowerInfo[0]));
        borrowerDTO.setFullName(borrowerInfo[1]);
        borrowerDTO.setEmail(borrowerInfo[2]);
        borrowerDTO.setContactNo(borrowerInfo[3]);
        borrowerDTO.setBorrowerType(b);
        return borrowerDTO;
    }

    public String convertBorrowerToJsonString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(borrowerDTO);
        return json;
    }
    
    public void deleteBorrower() throws Exception{
          HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response = httpClient.send(apiHelper.delete(url + idNo),
                HttpResponse.BodyHandlers.ofString());
    }

}
