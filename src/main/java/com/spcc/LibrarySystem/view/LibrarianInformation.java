/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package com.spcc.LibrarySystem.view;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spcc.LibrarySystem.dto.LibrarianDTO;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class LibrarianInformation extends javax.swing.JFrame {
    
    private final String SERVER_ERROR = "Unabled to connect to the server";
    private final String UPDATE_SUCCESS = "Data has successfully updated";
    private final String DELETE_SUCCESS = "Data has successfully deleted";
    
    
    public LibrarianInformation() throws Exception {
        initComponents();
        showLibrarian();
    }

    @SuppressWarnings("unchecked")

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblLibrarian = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(null);

        tblLibrarian.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "idNo", "fullName", "email", "password"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tblLibrarian.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tblLibrarian.setShowGrid(true);
        jScrollPane1.setViewportView(tblLibrarian);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(20, 10, 430, 220);

        jButton1.setText("Refresh");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);
        jButton1.setBounds(10, 250, 80, 40);

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        jPanel1.add(btnDelete);
        btnDelete.setBounds(190, 250, 80, 40);

        jButton3.setText("Update");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);
        jButton3.setBounds(100, 250, 80, 40);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 503, Short.MAX_VALUE)
                .addContainerGap())
        );

        setBounds(0, 0, 481, 555);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        try {
            showLibrarian();
        } catch (Exception ex) {
          ex.printStackTrace();
        }

    }

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        try {
            sendLibrarianDeleteRequest();
            showLibrarian();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try {
            int responseCode = sendPutRequest().statusCode();
            if(responseCode == 200){
                showMessageDailogResponseStatus(responseCode,UPDATE_SUCCESS);
                showLibrarian();
            }else{
                showMessageDailogResponseStatus(responseCode,SERVER_ERROR);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void showLibrarian() throws Exception {
        DefaultTableModel model;
        model = (DefaultTableModel) tblLibrarian.getModel();
        refresh(model);
        for (var lang : sendLibrarianGetRequest()) 
            model.addRow(new Object[]{
                lang.getIdNo(),
                lang.getFullName(),
                lang.getEmail(),
                lang.getPassword()});
    }

    private DefaultTableModel refresh(DefaultTableModel model) {
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
        return model;
    }

    private LibrarianDTO[] sendLibrarianGetRequest() throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response = httpClient.send(getHttpRequest(),
                HttpResponse.BodyHandlers.ofString());
        String json = response.body();
        Gson gson = new Gson();
        LibrarianDTO[] librarian = gson.fromJson(json, LibrarianDTO[].class);
        System.out.print(response.statusCode());
        return librarian;
    }
    
    private HttpRequest getHttpRequest() throws Exception {
        HttpRequest getReQuest = HttpRequest.newBuilder()
                .uri(new URI("http://localhost:8080/librarian/"))
                .GET()
                .build();
        return getReQuest;
    }

    private void showMessageDailogResponseStatus(int statusCode,
            String message){
        if(statusCode == 200){
            JOptionPane.showMessageDialog(null, message,
                    "Success", JOptionPane.INFORMATION_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(null, message,
                    "ServerError",JOptionPane.ERROR_MESSAGE);
        }
    }

   
    private HttpRequest getdeleteHttpRequest() throws Exception {
        int tableColumnIndex = 0;
        int selectedLibrarianIdNo = tblLibrarian.getSelectedRow();
        String idNo = tblLibrarian.getModel()
                .getValueAt(selectedLibrarianIdNo, tableColumnIndex).toString();
        HttpRequest deleteReQuest = HttpRequest.newBuilder()
                .uri(new URI("http://localhost:8080/librarian/" + idNo))
                .DELETE()
                .build();
        return deleteReQuest;
    }

    private void sendLibrarianDeleteRequest() throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response = httpClient.send(getdeleteHttpRequest(),
                HttpResponse.BodyHandlers.ofString());
    }

    private LibrarianDTO setLibrarianInfo() {
        int selectedRowIndex = tblLibrarian.getSelectedRow();
        LibrarianDTO librarian = new LibrarianDTO();
        librarian.setFullName(selectedInput(selectedRowIndex, 1));
        librarian.setEmail(selectedInput(selectedRowIndex, 2));
        librarian.setPassword(selectedInput(selectedRowIndex, 3));
        return librarian;
    }
    
    private String selectedInput(int selectedRowIndex, int selectedColumnIndex) {
        DefaultTableModel model;
        model = (DefaultTableModel) tblLibrarian.getModel();
        return model.getValueAt(selectedRowIndex,
                selectedColumnIndex).toString();
    }
    
     private String parseLibrarianToJsonString() throws Exception{
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(setLibrarianInfo());
        return json;
    }
    
    private HttpRequest getPutHttpRequest() throws Exception {
        int tableColumnIndex = 0;
        int selectedLibrarianIdNo = tblLibrarian.getSelectedRow();
        String idNo = tblLibrarian.getModel()
                .getValueAt(selectedLibrarianIdNo, tableColumnIndex).toString();
        HttpRequest putReQuest = HttpRequest.newBuilder()
                .uri(new URI("http://localhost:8080/librarian/" + idNo))
                .header("Content-Type", "application/json; charset=utf8")
                .PUT(HttpRequest.BodyPublishers.ofString(parseLibrarianToJsonString()))
                .build();
        return putReQuest;
    }
    
    private HttpResponse sendPutRequest() throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> putResponse = httpClient.send(getPutHttpRequest(), 
        HttpResponse.BodyHandlers.ofString());
        return putResponse;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LibrarianInformation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LibrarianInformation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LibrarianInformation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LibrarianInformation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new LibrarianInformation().setVisible(true);
                } catch (Exception ex) {
                    Logger.getLogger(LibrarianInformation.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblLibrarian;
    // End of variables declaration//GEN-END:variables
}
