
package com.spcc.LibrarySystem.view.LibrarianRecord;

import com.spcc.LibrarySystem.view.BorrowerRecord.BookTransaction;
import com.spcc.LibrarySystem.view.BorrowerRecord.BorrowerInformation;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class BookInventory extends javax.swing.JFrame {

    private final String UPDATE_SUCCESS="Update success";
    private final String BOOK_ADDED_SUCCESS="Book added success";
    private final String DELETE_SUCCESS = "Delete success";
    
    private String borrowerIdNo;
    private String borrowerName;

    public String getBorrowerType() {
        return borrowerType;
    }

    public void setBorrowerType(String borrowerType) {
        this.borrowerType = borrowerType;
    }
    private String borrowerType;
    
    public String getBorrowerName() {
        return borrowerName;
    }

    public void setBorrowerName(String borrowerName) {
        this.borrowerName = borrowerName;
    }

    public String getBorrowerIdNo() {
        return borrowerIdNo;
    }

    public void setBorrowerIdNo(String borrowerIdNo) {
        this.borrowerIdNo = borrowerIdNo;
    }
   
    public BookInventory() throws Exception {
        initComponents();
        showBook();
        generateAuthor();
        generateGenre();
        generatePublisher();
      
    }

    @SuppressWarnings("unchecked")

    private void showBook() throws Exception {
        BookInventoryClass books = new BookInventoryClass();
        DefaultTableModel model = (DefaultTableModel) tblInventory.getModel();
        refresh(model);
        for (var book : books.sendBookGetRequest()) {
            String bookAuthor = (book.getAuthors().size() == 0)
                    ? " " : book.getAuthors().get(0).getIdNo() + ":"
                    + book.getAuthors().get(0).getName();
            String bookGenre = (book.getGenres().size() == 0)
                    ? " " : book.getGenres().get(0).getIdNo()
                    + ":" + book.getGenres().get(0).getName();
            model.addRow(new Object[]{
                book.getIdNo(),
                book.getName(),
                book.getQty(),
                book.getDatePublished(),
                bookAuthor,
                book.getPublisher().getIdNo() + ":"
                + book.getPublisher().getName(),
                bookGenre
            });
        }

    }

    private DefaultTableModel refresh(DefaultTableModel model) {
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
        return model;
    }

    private void generateAuthor() throws Exception {
        BookInventoryClass author = new BookInventoryClass();
        DefaultComboBoxModel cmbAuthorModel = new DefaultComboBoxModel();
        for (var a : author.sendGetAuthorRequest()) {
            cmbAuthorModel.addElement(a.getIdNo() + ":" + a.getName());
        }
        cmbAuthor.setModel(cmbAuthorModel);
    }

    private void generateGenre() throws Exception {
        BookInventoryClass genre = new BookInventoryClass();
        DefaultComboBoxModel cmbGenreModel = new DefaultComboBoxModel();
        for (var a : genre.sendGetGenreRequest()) {
            cmbGenreModel.addElement(a.getIdNo() + ":" + a.getName());
        }
        cmbGenre.setModel(cmbGenreModel);
    }

    private void generatePublisher() throws Exception {
        BookInventoryClass publisher = new BookInventoryClass();
        DefaultComboBoxModel cmbPublisherModel = new DefaultComboBoxModel();
        for (var a : publisher.sendGetPublisherRequest()) {
            cmbPublisherModel.addElement(a.getIdNo() + ":" + a.getName());
        }
        cmbPublisher.setModel(cmbPublisherModel);
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        InventoryPanel = new javax.swing.JPanel();
        txtBookTitle = new javax.swing.JTextField();
        txtid = new javax.swing.JTextField();
        txtQuantity = new javax.swing.JTextField();
        lblPublisher = new javax.swing.JLabel();
        lblid = new javax.swing.JLabel();
        lblBookTitle = new javax.swing.JLabel();
        lblQuantity = new javax.swing.JLabel();
        lblDatePublish = new javax.swing.JLabel();
        lblAuthor = new javax.swing.JLabel();
        txtGenre = new javax.swing.JTextField();
        cmbGenre = new javax.swing.JComboBox<>();
        btnDelete = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        lblPrice = new javax.swing.JLabel();
        txtAuthor = new javax.swing.JTextField();
        cmbAuthor = new javax.swing.JComboBox<>();
        txtDate = new javax.swing.JTextField();
        cmbPublisher = new javax.swing.JComboBox<>();
        btnBack = new javax.swing.JButton();
        Panel1 = new javax.swing.JScrollPane();
        tblInventory = new javax.swing.JTable();
        Background = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setUndecorated(true);
        setResizable(false);
        setType(java.awt.Window.Type.POPUP);

        jPanel1.setToolTipText("");
        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jPanel1.setInheritsPopupMenu(true);
        jPanel1.setMaximumSize(new java.awt.Dimension(876, 490));
        jPanel1.setMinimumSize(new java.awt.Dimension(876, 490));
        jPanel1.setPreferredSize(new java.awt.Dimension(876, 490));
        jPanel1.setLayout(null);

        jPanel2.setLayout(null);

        InventoryPanel.setBackground(new java.awt.Color(255, 255, 255));
        InventoryPanel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));
        InventoryPanel.setOpaque(false);
        InventoryPanel.setLayout(null);

        txtBookTitle.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        InventoryPanel.add(txtBookTitle);
        txtBookTitle.setBounds(40, 100, 210, 30);

        txtid.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        InventoryPanel.add(txtid);
        txtid.setBounds(40, 40, 210, 30);

        txtQuantity.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        InventoryPanel.add(txtQuantity);
        txtQuantity.setBounds(40, 160, 210, 30);

        lblPublisher.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        lblPublisher.setForeground(new java.awt.Color(255, 255, 255));
        lblPublisher.setText("Publisher:");
        InventoryPanel.add(lblPublisher);
        lblPublisher.setBounds(20, 320, 90, 17);

        lblid.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        lblid.setForeground(new java.awt.Color(255, 255, 255));
        lblid.setText("ID:");
        InventoryPanel.add(lblid);
        lblid.setBounds(20, 20, 50, 17);

        lblBookTitle.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        lblBookTitle.setForeground(new java.awt.Color(255, 255, 255));
        lblBookTitle.setText("Book Title:");
        InventoryPanel.add(lblBookTitle);
        lblBookTitle.setBounds(20, 80, 130, 17);

        lblQuantity.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        lblQuantity.setForeground(new java.awt.Color(255, 255, 255));
        lblQuantity.setText("Quantity: ");
        InventoryPanel.add(lblQuantity);
        lblQuantity.setBounds(20, 140, 100, 17);

        lblDatePublish.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        lblDatePublish.setForeground(new java.awt.Color(255, 255, 255));
        lblDatePublish.setText("Date Publish: ");
        InventoryPanel.add(lblDatePublish);
        lblDatePublish.setBounds(20, 200, 130, 17);

        lblAuthor.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        lblAuthor.setForeground(new java.awt.Color(255, 255, 255));
        lblAuthor.setText("Author:");
        InventoryPanel.add(lblAuthor);
        lblAuthor.setBounds(20, 260, 110, 17);

        txtGenre.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        InventoryPanel.add(txtGenre);
        txtGenre.setBounds(40, 400, 210, 30);

        cmbGenre.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        cmbGenre.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbGenreItemStateChanged(evt);
            }
        });
        InventoryPanel.add(cmbGenre);
        cmbGenre.setBounds(40, 400, 210, 30);

        btnDelete.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btnDelete.setText("DELETE");
        btnDelete.setEnabled(false);
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        InventoryPanel.add(btnDelete);
        btnDelete.setBounds(60, 500, 150, 30);

        btnAdd.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btnAdd.setText("ADD");
        btnAdd.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 0, true));
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        InventoryPanel.add(btnAdd);
        btnAdd.setBounds(20, 450, 100, 30);

        btnUpdate.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        btnUpdate.setText("UPDATE");
        btnUpdate.setEnabled(false);
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });
        InventoryPanel.add(btnUpdate);
        btnUpdate.setBounds(150, 450, 100, 30);

        lblPrice.setFont(new java.awt.Font("Century Gothic", 1, 13)); // NOI18N
        lblPrice.setForeground(new java.awt.Color(255, 255, 255));
        lblPrice.setText("Genre:");
        InventoryPanel.add(lblPrice);
        lblPrice.setBounds(20, 380, 90, 17);

        txtAuthor.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        txtAuthor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAuthorActionPerformed(evt);
            }
        });
        InventoryPanel.add(txtAuthor);
        txtAuthor.setBounds(40, 280, 210, 30);

        cmbAuthor.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        cmbAuthor.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbAuthorItemStateChanged(evt);
            }
        });
        cmbAuthor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbAuthorActionPerformed(evt);
            }
        });
        InventoryPanel.add(cmbAuthor);
        cmbAuthor.setBounds(40, 280, 210, 30);

        txtDate.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        InventoryPanel.add(txtDate);
        txtDate.setBounds(40, 220, 210, 30);

        cmbPublisher.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        InventoryPanel.add(cmbPublisher);
        cmbPublisher.setBounds(40, 340, 210, 30);

        jPanel2.add(InventoryPanel);
        InventoryPanel.setBounds(10, 50, 270, 540);

        btnBack.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btnBack.setText("BACK");
        btnBack.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 0, true));
        btnBack.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });
        jPanel2.add(btnBack);
        btnBack.setBounds(10, 10, 60, 30);

        tblInventory.setBackground(new java.awt.Color(238, 238, 238));
        tblInventory.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        tblInventory.setFont(new java.awt.Font("Century", 0, 12)); // NOI18N
        tblInventory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Book Title", "Quantity", "Date Publish", "Author", "Publisher", "Genre"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblInventory.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblInventoryMouseClicked(evt);
            }
        });
        Panel1.setViewportView(tblInventory);
        if (tblInventory.getColumnModel().getColumnCount() > 0) {
            tblInventory.getColumnModel().getColumn(0).setHeaderValue("ID");
            tblInventory.getColumnModel().getColumn(1).setHeaderValue("Book Title");
            tblInventory.getColumnModel().getColumn(2).setHeaderValue("Quantity");
            tblInventory.getColumnModel().getColumn(3).setHeaderValue("Date Publish");
            tblInventory.getColumnModel().getColumn(4).setHeaderValue("Author");
            tblInventory.getColumnModel().getColumn(5).setHeaderValue("Publisher");
            tblInventory.getColumnModel().getColumn(6).setHeaderValue("Genre");
        }

        jPanel2.add(Panel1);
        Panel1.setBounds(320, 140, 670, 430);

        Background.setIcon(new javax.swing.ImageIcon("C:\\Users\\admin\\Desktop\\LibrarySystem\\src\\main\\java\\com\\spcc\\LibrarySystem\\assets\\Background_Inventory.PNG")); // NOI18N
        Background.setText(".");
        jPanel2.add(Background);
        Background.setBounds(0, -20, 1110, 640);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(0, 0, 1000, 600);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1000, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        if (btnBack.getText().equals("CANCEL")) {
            try {
                new BorrowerInformation().setVisible(true);
                setVisible(false);
            } catch (Exception ex) {
                Logger.getLogger(BookInventory.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            new Dashboard().setVisible(true);
            setVisible(false);
        }

    }//GEN-LAST:event_btnBackActionPerformed

    private void cmbGenreItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbGenreItemStateChanged
        System.out.print(cmbGenre.getSelectedIndex());
    }//GEN-LAST:event_cmbGenreItemStateChanged

    private void cmbAuthorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbAuthorItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbAuthorItemStateChanged

    private void cmbAuthorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbAuthorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbAuthorActionPerformed

    private void tblInventoryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblInventoryMouseClicked
        disibledTxtAuthorAndTxtGenre();
        enabledDeleteAndUpdateButton();
        disibledBtnAdd();
        DefaultTableModel model = (DefaultTableModel) tblInventory.getModel();
        int selectedRowIndex = tblInventory.getSelectedRow();
        int[] columnIndex = {0, 1, 2, 3, 4, 5, 6};
        txtid.setText(model.getValueAt(
                selectedRowIndex, columnIndex[0]).toString());
        txtBookTitle.setText(model.getValueAt(
                selectedRowIndex, columnIndex[1]).toString());
        txtQuantity.setText(model.getValueAt(
                selectedRowIndex, columnIndex[2]).toString());
        txtDate.setText(model.getValueAt(
                selectedRowIndex, columnIndex[3]).toString());
        cmbAuthor.setSelectedItem(model.getValueAt(
                selectedRowIndex, columnIndex[4]));
        cmbPublisher.setSelectedItem(model.getValueAt(
                selectedRowIndex, columnIndex[5]));
        cmbGenre.setSelectedItem(model.getValueAt(
                selectedRowIndex, columnIndex[6]));
    }//GEN-LAST:event_tblInventoryMouseClicked

    private void disibledTxtAuthorAndTxtGenre() {
        txtAuthor.setVisible(false);
        txtGenre.setVisible(false);
    }

    public void enabledDeleteAndUpdateButton() {
        btnUpdate.setEnabled(true);
        btnDelete.setEnabled(true);
    }

    public void disibledBtnAdd() {
        btnAdd.setEnabled(false);
    }

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt)  {//GEN-FIRST:event_btnAddActionPerformed
        BookInventoryClass book = new BookInventoryClass();
        String[] data = {
            txtBookTitle.getText(),
            txtQuantity.getText(),
            txtDate.getText(),
            txtGenre.getText(),
            txtAuthor.getText(),
            getPublisherIdNo(),};
        book.setBorrowerAddInfo(data);
        try {
            book.sendPostRequest();
            showBook();
            clearText();
            disibledBtnAdd();
            disibledTxtAuthorAndTxtGenre();
            showSuccessMessage(BOOK_ADDED_SUCCESS);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }//GEN-LAST:event_btnAddActionPerformed

    
    private void showSuccessMessage(String message){
        JOptionPane.showMessageDialog(null, message,
                   "Success" , JOptionPane.INFORMATION_MESSAGE);
    }
    
    
    private String getPublisherIdNo() {
        String[] publisher = cmbPublisher.getSelectedItem()
                .toString().split(":", 3);
        return publisher[0];
    }

    private void clearText() {
        txtBookTitle.setText("");
        txtQuantity.setText("");
        txtDate.setText("");
        txtGenre.setText("");
        txtAuthor.setText("");
    }

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        BookInventoryClass book = new BookInventoryClass();
        String[] data = {
            txtBookTitle.getText(),
            txtQuantity.getText(),
            txtDate.getText(),
            getGenreIdNo(),
            getGenreName(),
            getAuthorIdNo(),
            getAuthorName(),
            getPublisherIdNo(),
            getPublisherName(),};
        book.setBorrowerUpdateInfo(data);
        try {
            book.setIdNo(Integer.parseInt(txtid.getText()));
            book.sendPutRequest();
            showBook();
            clearText();
            disibledDeleteAndUpdateButton();
            enabledBtnAdd();
            enabledTxtAuthorAndTxtGenre();
            showSuccessMessage(BOOK_ADDED_SUCCESS);
        } catch (Exception e) {

        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private String getGenreIdNo() {
        String[] genre = cmbGenre.getSelectedItem().toString().split(":", 3);
        return genre[0];
    }

    private String getGenreName() {
        String[] genre = cmbGenre.getSelectedItem().toString().split(":", 3);
        return genre[1];
    }

    private String getAuthorIdNo() {
        String[] author = cmbAuthor.getSelectedItem().toString().split(":", 3);
        return author[0];
    }

    private String getAuthorName() {
        String[] author = cmbAuthor.getSelectedItem().toString().split(":", 3);
        return author[1];
    }

    private String getPublisherName() {
        String[] publisher = cmbPublisher.getSelectedItem().toString().split(":", 3);
        return publisher[1];
    }

    public void disibledDeleteAndUpdateButton() {
        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
    }

    public void enabledBtnAdd() {
        btnAdd.setEnabled(true);
    }

    private void enabledTxtAuthorAndTxtGenre() {
        txtAuthor.setVisible(true);
        txtGenre.setVisible(true);
    }

    private void txtAuthorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAuthorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAuthorActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        BookInventoryClass deleteBook = new BookInventoryClass();
        if (btnDelete.getText().equals("DELETE")) {
            try {
                deleteBook.setIdNo(Integer.parseInt(txtid.getText()));
                deleteBook.sendDeleteRequest();
                JOptionPane.showMessageDialog(null, DELETE_SUCCESS,
                        "Success", JOptionPane.INFORMATION_MESSAGE);
                showBook();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Not connected to Server",
                        "Server offline", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            BorrowBookClass borrowBook = new BorrowBookClass();
            BookTransaction booktransaction = new BookTransaction();
            String[] data = {
                txtDate.getText(),
                txtQuantity.getText(),
                getBorrowerIdNo(),
                txtid.getText(),};
            borrowBook.setTransactionInfo(data);
            try {
                borrowBook.sendPostRequest();
                JOptionPane.showMessageDialog(null, "Borrow Success",
                        "Success", JOptionPane.INFORMATION_MESSAGE);
                booktransaction.lblDate.setText(txtDate.getText());
                booktransaction.lblFullName.setText(getBorrowerName());
                booktransaction.lblType.setText(getBorrowerType());
                setTransactionTable(booktransaction);
                booktransaction.setVisible(true);
                setVisible(false);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Not connected to Server",
                        "Server offline", JOptionPane.ERROR_MESSAGE);
            }
        }

    }//GEN-LAST:event_btnDeleteActionPerformed

    private void setTransactionTable(BookTransaction booktransaction) {
        DefaultTableModel model = (DefaultTableModel) 
                booktransaction.jtableBook.getModel();
        model.addRow(new Object[]{
            txtid.getText(),
            txtBookTitle.getText(),
            txtQuantity.getText(),
            txtDate.getText(),
        });
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BookInventory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BookInventory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BookInventory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BookInventory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new BookInventory().setVisible(true);
                } catch (Exception ex) {
                    ex.printStackTrace();
                  JOptionPane.showMessageDialog(null,"Not connected to Server",
                    "Server offline", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JLabel Background;
    public javax.swing.JPanel InventoryPanel;
    private javax.swing.JScrollPane Panel1;
    public javax.swing.JButton btnAdd;
    public javax.swing.JButton btnBack;
    public javax.swing.JButton btnDelete;
    public javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox<String> cmbAuthor;
    private javax.swing.JComboBox<String> cmbGenre;
    private javax.swing.JComboBox<String> cmbPublisher;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblAuthor;
    private javax.swing.JLabel lblBookTitle;
    public javax.swing.JLabel lblDatePublish;
    private javax.swing.JLabel lblPrice;
    private javax.swing.JLabel lblPublisher;
    private javax.swing.JLabel lblQuantity;
    private javax.swing.JLabel lblid;
    private javax.swing.JTable tblInventory;
    private javax.swing.JTextField txtAuthor;
    private javax.swing.JTextField txtBookTitle;
    private javax.swing.JTextField txtDate;
    private javax.swing.JTextField txtGenre;
    private javax.swing.JTextField txtQuantity;
    private javax.swing.JTextField txtid;
    // End of variables declaration//GEN-END:variables
}
