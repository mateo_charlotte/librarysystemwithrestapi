/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.spcc.LibrarySystem.view.LibrarianRecord;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spcc.LibrarySystem.ApiHelper.ApiHelper;
import com.spcc.LibrarySystem.dto.AuthorDTO;
import com.spcc.LibrarySystem.dto.BookDTO;
import com.spcc.LibrarySystem.dto.GenreDTO;
import com.spcc.LibrarySystem.dto.PublisherDTO;
import com.spcc.LibrarySystem.model.Author;
import com.spcc.LibrarySystem.model.Genre;
import com.spcc.LibrarySystem.model.Publisher;

import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author admin
 */
public class BookInventoryClass {

    private final ApiHelper apiHelper = new ApiHelper();
    private final String url = "http://localhost:8080/book/";
    private final String urlAuthor = "http://localhost:8080/author/";
    private final String urlGenre = "http://localhost:8080/genre/";
    private final String urlPubliser = "http://localhost:8080/publisher/";
    private final BookDTO bookDTO = new BookDTO();

    private int idNo;

    public void setIdNo(int idNo) {
        this.idNo = idNo;
    }

    public BookDTO[] sendBookGetRequest() throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response = httpClient.send(apiHelper.get(url),
                HttpResponse.BodyHandlers.ofString());
        String json = response.body();
        Gson gson = new Gson();
        BookDTO[] book = gson.fromJson(json, BookDTO[].class);
        return book;
    }

    public AuthorDTO[] sendGetAuthorRequest() throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response = httpClient.send(apiHelper.get(urlAuthor),
                HttpResponse.BodyHandlers.ofString());
        String json = response.body();
        Gson gson = new Gson();
        AuthorDTO[] book = gson.fromJson(json, AuthorDTO[].class);
        return book;
    }

    public GenreDTO[] sendGetGenreRequest() throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response = httpClient.send(apiHelper.get(urlGenre),
                HttpResponse.BodyHandlers.ofString());
        String json = response.body();
        Gson gson = new Gson();
        GenreDTO[] genre = gson.fromJson(json, GenreDTO[].class);
        return genre;
    }

    public PublisherDTO[] sendGetPublisherRequest() throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response = httpClient.send(apiHelper.get(urlPubliser),
                HttpResponse.BodyHandlers.ofString());
        String json = response.body();
        Gson gson = new Gson();
        PublisherDTO[] publisher = gson.fromJson(json, PublisherDTO[].class);
        return publisher;
    }

    public BookDTO setBorrowerUpdateInfo(String[] bookInfo) {
        Author author = new Author();
        author.setIdNo(Integer.parseInt(bookInfo[5]));
        author.setName(bookInfo[6]);
        List<Author> authors = new ArrayList<>();
        authors.add(author);
        Genre genre = new Genre();
        genre.setIdNo(Integer.parseInt(bookInfo[3]));
        genre.setName(bookInfo[4]);
        List<Genre> genres = new ArrayList<>();
        genres.add(genre);
        Publisher publisher = new Publisher();
        publisher.setIdNo(Integer.parseInt(bookInfo[7]));
        bookDTO.setName(bookInfo[0]);
        bookDTO.setQty(Integer.parseInt(bookInfo[1]));
        bookDTO.setGenres(genres);
        bookDTO.setAuthors(authors);
        bookDTO.setPublisher(publisher);
        return bookDTO;
    }

    public BookDTO setBorrowerAddInfo(String[] bookInfo) {
        Author author = new Author();
        author.setName(bookInfo[4]);
        List<Author> authors = new ArrayList<>();
        authors.add(author);
        Genre genre = new Genre();
        genre.setName(bookInfo[3]);
        List<Genre> genres = new ArrayList<>();
        genres.add(genre);
        Publisher publisher = new Publisher();
        publisher.setIdNo(Integer.parseInt(bookInfo[5]));
        bookDTO.setName(bookInfo[0]);
        bookDTO.setQty(Integer.parseInt(bookInfo[1]));
        bookDTO.setGenres(genres);
        bookDTO.setAuthors(authors);
        bookDTO.setPublisher(publisher);
        return bookDTO;
    }

    public String convertBookToJsonString() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(bookDTO);
        return json;
    }

    public void sendPostRequest() throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        httpClient.send(apiHelper.post(url, convertBookToJsonString()),
                HttpResponse.BodyHandlers.ofString());
    }

    public void sendPutRequest() throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        httpClient.send(apiHelper.put(url + idNo, convertBookToJsonString()),
                HttpResponse.BodyHandlers.ofString());
    }

    public void sendDeleteRequest() throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        httpClient.send(apiHelper.delete(url + idNo),
                HttpResponse.BodyHandlers.ofString());
    }

}
