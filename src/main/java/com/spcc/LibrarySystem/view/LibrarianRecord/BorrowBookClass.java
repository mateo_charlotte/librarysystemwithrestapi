
package com.spcc.LibrarySystem.view.LibrarianRecord;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spcc.LibrarySystem.ApiHelper.ApiHelper;
import com.spcc.LibrarySystem.dto.TransactionDTO;
import com.spcc.LibrarySystem.model.Book;
import com.spcc.LibrarySystem.model.Borrower;
import com.spcc.LibrarySystem.model.Librarian;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;

public class BorrowBookClass {
    private final ApiHelper apiHelper = new ApiHelper();
    
    private final String url = "http://localhost:8080/transaction/";
    
    private final TransactionDTO borrowerDTO = new TransactionDTO();
    
    
      public TransactionDTO[] sendTransactionGetRequest() throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response = httpClient.send(apiHelper.get(url),
                HttpResponse.BodyHandlers.ofString());
        String json = response.body();
Gson gson = new GsonBuilder()
       .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
       .create();
        TransactionDTO[] transaction = gson.fromJson(json, TransactionDTO[].class);
        return transaction;
    }
    
    public void setTransactionInfo(String [] data) { 
        Borrower borrower = new Borrower();
        borrower.setIdNo(Integer.parseInt(data[2]));
        Book book = new Book();
        book.setIdNo(Integer.parseInt(data[3]));
        Librarian librarian = new Librarian();
        librarian.setIdNo(1);
//        borrowerDTO.setReturnDate(Date.from(Instant.now()));
        borrowerDTO.setQty(Integer.parseInt(data[1]));
        borrowerDTO.setBorrower(borrower);
        borrowerDTO.setBook(book);
        borrowerDTO.setLibrarian(librarian);
    }
    
      public String convertTransactionToJsonString() {
        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(borrowerDTO);
        return json;
    }

    public void sendPostRequest() throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        httpClient.send(apiHelper.post(url, convertTransactionToJsonString()),
                HttpResponse.BodyHandlers.ofString());
    }
    
}
