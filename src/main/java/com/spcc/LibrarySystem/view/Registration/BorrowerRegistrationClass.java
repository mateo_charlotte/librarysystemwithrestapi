
package com.spcc.LibrarySystem.view.Registration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spcc.LibrarySystem.ApiHelper.ApiHelper;
import com.spcc.LibrarySystem.dto.BorrowerDTO;
import com.spcc.LibrarySystem.model.BorrowerType;

import java.net.http.HttpClient;
import java.net.http.HttpResponse;

public class BorrowerRegistrationClass {

    private final ApiHelper apiHelper = new ApiHelper();
    private final String url = "http://localhost:8080/borrower/";
    private final BorrowerDTO borrowerDTO = new BorrowerDTO();

  
    public BorrowerDTO setBorrowerInfo(String[] borrowerInfo, int borrowerTypeIdNo) {
        BorrowerType b = new BorrowerType();
        b.setIdNo(borrowerTypeIdNo + 1);
        borrowerDTO.setFullName(borrowerInfo[0]);
        borrowerDTO.setEmail(borrowerInfo[1]);
        borrowerDTO.setContactNo(borrowerInfo[2]);
        borrowerDTO.setBorrowerType(b);
        return borrowerDTO;
    }

    public String convertBorrowerToJsonString(){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(borrowerDTO);
        return json;
    }

    public void sendPostRequest() throws Exception {
        HttpClient httpClient = HttpClient.newHttpClient();
        httpClient.send(apiHelper.post(url,convertBorrowerToJsonString()),
                HttpResponse.BodyHandlers.ofString());
    }
    
    
    
}
