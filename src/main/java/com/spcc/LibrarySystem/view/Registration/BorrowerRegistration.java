/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spcc.LibrarySystem.view.Registration;

import com.spcc.LibrarySystem.view.LibrarianRecord.Dashboard;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

;

public class BorrowerRegistration extends javax.swing.JFrame {

    private final BorrowerRegistrationClass borrower = new BorrowerRegistrationClass();

    public BorrowerRegistration() throws Exception {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblFullName = new javax.swing.JLabel();
        txtFullName = new javax.swing.JTextField();
        lblEmailAddress = new javax.swing.JLabel();
        txtEmailAddress = new javax.swing.JTextField();
        lblContactNo = new javax.swing.JLabel();
        txtContactNumber = new javax.swing.JTextField();
        lblBorrowedType = new javax.swing.JLabel();
        cmbBorrowedType = new javax.swing.JComboBox<>();
        BorrowerReg_Panel = new javax.swing.JPanel();
        btnRegister = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(925, 691));
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(null);

        jPanel1.setPreferredSize(new java.awt.Dimension(748, 562));
        jPanel1.setLayout(null);

        lblFullName.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        lblFullName.setForeground(new java.awt.Color(255, 255, 255));
        lblFullName.setText("Full Name:");
        jPanel1.add(lblFullName);
        lblFullName.setBounds(520, 280, 120, 19);

        txtFullName.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtFullName.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtFullName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtFullNameActionPerformed(evt);
            }
        });
        jPanel1.add(txtFullName);
        txtFullName.setBounds(520, 300, 270, 40);

        lblEmailAddress.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        lblEmailAddress.setForeground(new java.awt.Color(255, 255, 255));
        lblEmailAddress.setText("Email Address:");
        jPanel1.add(lblEmailAddress);
        lblEmailAddress.setBounds(520, 350, 120, 19);

        txtEmailAddress.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtEmailAddress.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtEmailAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEmailAddressActionPerformed(evt);
            }
        });
        jPanel1.add(txtEmailAddress);
        txtEmailAddress.setBounds(520, 370, 270, 40);

        lblContactNo.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        lblContactNo.setForeground(new java.awt.Color(255, 255, 255));
        lblContactNo.setText("Contact Number:");
        jPanel1.add(lblContactNo);
        lblContactNo.setBounds(520, 490, 120, 19);

        txtContactNumber.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtContactNumber.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        txtContactNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtContactNumberActionPerformed(evt);
            }
        });
        jPanel1.add(txtContactNumber);
        txtContactNumber.setBounds(520, 510, 270, 40);

        lblBorrowedType.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        lblBorrowedType.setForeground(new java.awt.Color(255, 255, 255));
        lblBorrowedType.setText("Borrowed Type:");
        jPanel1.add(lblBorrowedType);
        lblBorrowedType.setBounds(520, 420, 120, 19);

        cmbBorrowedType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Student ", "Teacher", "Staff" }));
        jPanel1.add(cmbBorrowedType);
        cmbBorrowedType.setBounds(520, 440, 270, 40);

        BorrowerReg_Panel.setBackground(new java.awt.Color(255, 255, 255));
        BorrowerReg_Panel.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 1, true));
        BorrowerReg_Panel.setOpaque(false);
        BorrowerReg_Panel.setLayout(null);

        btnRegister.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btnRegister.setText("REGISTER");
        btnRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterActionPerformed(evt);
            }
        });
        BorrowerReg_Panel.add(btnRegister);
        btnRegister.setBounds(230, 400, 130, 40);

        btnCancel.setFont(new java.awt.Font("Century Gothic", 1, 14)); // NOI18N
        btnCancel.setText("CANCEL");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        BorrowerReg_Panel.add(btnCancel);
        btnCancel.setBounds(90, 400, 130, 40);

        jPanel1.add(BorrowerReg_Panel);
        BorrowerReg_Panel.setBounds(430, 160, 450, 480);

        jLabel1.setIcon(new javax.swing.ImageIcon("C:\\Users\\admin\\Desktop\\LibrarySystem\\src\\main\\java\\com\\spcc\\LibrarySystem\\assets\\background_BReg.PNG")); // NOI18N
        jPanel1.add(jLabel1);
        jLabel1.setBounds(0, 0, 1130, 700);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 930, 700);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txtFullNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFullNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFullNameActionPerformed

    private void txtEmailAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEmailAddressActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEmailAddressActionPerformed

    private void txtContactNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtContactNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtContactNumberActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        if(btnCancel.getText().equals("BACK")){
            setVisible(false);
            new Dashboard().setVisible(true);
        }else{
            clearText();
        }
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnRegisterActionPerformed(java.awt.event.ActionEvent evt) {
        try {
            if (!hasData()) {
                String[] data = {
                    txtFullName.getText(),
                    txtEmailAddress.getText(),
                    txtContactNumber.getText(),};
                borrower.setBorrowerInfo(
                        data, cmbBorrowedType.getSelectedIndex());
                borrower.sendPostRequest();
                JOptionPane.showMessageDialog(null, "Register Success",
                        "Success", JOptionPane.INFORMATION_MESSAGE);
                clearText();
            } else {
                JOptionPane.showMessageDialog(null, "fill-up all text field",
                        "Registration field", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Not connected to Server",
                    "Server offline", JOptionPane.ERROR_MESSAGE);
        }
    }

    private boolean hasData() {
        return txtFullName.getText().isEmpty()
                && txtEmailAddress.getText().isEmpty()
                && txtContactNumber.getText().isEmpty();
    }

    private void clearText() {
        txtFullName.setText("");
        txtEmailAddress.setText("");
        txtContactNumber.setText("");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BorrowerRegistration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BorrowerRegistration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BorrowerRegistration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BorrowerRegistration.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new BorrowerRegistration().setVisible(true);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Not connected to Server",
                            "Server offline", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BorrowerReg_Panel;
    public javax.swing.JButton btnCancel;
    private javax.swing.JButton btnRegister;
    private javax.swing.JComboBox<String> cmbBorrowedType;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblBorrowedType;
    private javax.swing.JLabel lblContactNo;
    private javax.swing.JLabel lblEmailAddress;
    private javax.swing.JLabel lblFullName;
    private javax.swing.JTextField txtContactNumber;
    private javax.swing.JTextField txtEmailAddress;
    private javax.swing.JTextField txtFullName;
    // End of variables declaration//GEN-END:variables
}
